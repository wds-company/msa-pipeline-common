#!/bin/sh
rm -rf /app
mkdir -p /app
bee pack -p /go/src/techberry-go/apis -o /app
cd /app
tar -zxvf apis.tar.gz