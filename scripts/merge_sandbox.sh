#!/bin/sh

export SOURCE_PATH=$1
export DEST_PATH=$2
export exclude_dir1="./.git/*"
export exclude_file1="service_controller.go"
export exclude_file2="service_accessor.go"
export exclude_file3="bind_input_root.go"
export exclude_file4="service_common.go"
echo "Merge sandbox script is execute."
cd $SOURCE_PATH/accessors
find . -type f \( -iname "*.*" ! -path "$exclude_dir1" ! -name "$exclude_file1" ! -name "$exclude_file2" ! -name "$exclude_file3" ! -name "$exclude_file4" \) -print0 -prune -o -depth | cpio -0pdv --quiet $DEST_PATH/accessors
