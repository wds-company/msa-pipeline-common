
def execute() {
    def connection_url = deployConfig['mongodb']['connection_url']
    def init = deployConfig['mongodb']['init']
    def enable = deployConfig['mongodb']["enable"]
    if (connection_url) {
        if (init) {
            def variable = "//<init.mongodb>"
            def value = init.replace("#connection_url#", connection_url)
            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """  
        }
        if (enable) {
            def variable = "//<enable.mongodb>"
            def value = enable
            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """                              
        } 
    } else {
        def variable = "//<init.mongodb>"
        def value = ""
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
        """ 
        variable = "//<enable.mongodb>"
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
        """
    }
}

return this