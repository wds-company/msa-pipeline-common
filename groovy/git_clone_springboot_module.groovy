
def execute(moduleArray) {
    for(module in moduleArray) {
        def moduleRepository = module.repository
        def modulePath = module.path
        def moduleVersion = module.version
        println("Clone module source for " + moduleRepository)
        sh "git clone ssh://git@bitbucket.beebuddy.net:7999/${moduleRepository} ${env.WORKSPACE}/${modulePath}"      
    }
}

return this