
def execute(configFile) {
    def exists = fileExists "deploy_config/${configFile}.json"
    if (exists) {
        echo "deploy_config/${configFile}.json found"
    } else {
        error("deploy_config/${configFile}.json cannot be found")
    }
    def files = null
    try {
        deployConfig = readJSON file: "deploy_config/${configFile}.json"
        return deployConfig
    } catch (Exception e) {
        error("Cannot read deploy_config/${configFile}.json file.\nError:\n${e}")
    }
}

return this