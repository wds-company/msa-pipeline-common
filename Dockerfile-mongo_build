# Parent Dockerfile https://github.com/docker-library/mongo/blob/982328582c74dd2f0a9c8c77b84006f291f974c3/3.0/Dockerfile
# Create build image
#########################################################
FROM mongo:4.0 as build

ARG db_name="newdb"
ENV DBNAME=${db_name}

# Modify child mongo to use /data/db2 as dbpath (because /data/db wont persist the build)
RUN mkdir -p /data/db2 \
    && mkdir -p /data/scripts \
    && echo "dbpath = /data/db2" > /etc/mongodb.conf \
    && chown -R mongodb:mongodb /data

COPY ./dbscripts/* /data/scripts/

RUN mongod --fork --logpath /var/log/mongodb.log --dbpath /data/db2 --smallfiles \
    && ADMIN_USER_FILE=/data/scripts/admin_user.js \
    && for f in $ADMIN_USER_FILE; do mongo 127.0.0.1:27017/admin $f; done \
    && DMA_USER_FILE=/data/scripts/dbapp_user.js \
    && for f in $DMA_USER_FILE; do mongo 127.0.0.1:27017/${db_name} $f; done \
    && CREATE_FILES=/data/scripts/*_create.js \
    && for f in $CREATE_FILES; do mongo 127.0.0.1:27017/${db_name} $f; done \
    && mongod --dbpath /data/db2 --shutdown \
    && chown -R mongodb /data/db2

# Make the new dir a VOLUME to persists it
#VOLUME /data/db2

#########################################################
# Create run image
#########################################################
FROM mongo:4.0

RUN mkdir -p /data/db2 \
    && mkdir -p /data/scripts \
    && echo "dbpath = /data/db2" > /etc/mongodb.conf \
    && chown -R mongodb:mongodb /data

COPY --from=build /data/db2 /data/db2

CMD ["mongod", "--config", "/etc/mongodb.conf", "--smallfiles", "--auth"]